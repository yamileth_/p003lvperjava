package com.example.p003lvperjava;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterAlumno extends RecyclerView.Adapter<AdapterAlumno.ViewHolder> implements View.OnClickListener, Filterable {

    protected ArrayList<AlumnoItem> listaAlumnos;
    private ArrayList<AlumnoItem> listaFinal;
    private View.OnClickListener listener;
    private Application context;
    private LayoutInflater inflater;

    public AdapterAlumno(ArrayList<AlumnoItem> listaAlumnos, Application context) {
        this.listaAlumnos = new ArrayList<>();
        this.listaAlumnos.addAll(listaAlumnos);
        this.listaFinal = listaAlumnos;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_alumno, null);
        view.setOnClickListener(this);

        return new ViewHolder(view);
    }

    private boolean isUriValid(String uriString) {
        return uriString != null && !uriString.isEmpty();
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterAlumno.ViewHolder holder, int position) {
        AlumnoItem alumno = this.listaFinal.get(position);
        holder.txtMatricula.setText(alumno.getMatricula());
        holder.txtNombre.setText(alumno.getNombre());
        holder.txtCarrera.setText(alumno.getCarrera());
        if (isUriValid(alumno.getImg())) {
            Context context = holder.itemView.getContext();
            Glide.with(context)
                    .load(Uri.parse(alumno.getImg()))
                    .placeholder(R.drawable.generaluser)
                    .into(holder.idImagen);
        } else {
            holder.idImagen.setImageResource(R.drawable.generaluser);
        }

    }

    @Override
    public int getItemCount() {
        return listaFinal.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) listener.onClick(v);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence texto) {
                FilterResults resultado = new FilterResults();

                ArrayList<AlumnoItem> listaFiltrada = new ArrayList<>();

                if (texto != null && texto.length() != 0) {
                    String busqueda = texto.toString().toLowerCase().trim();
                    for (AlumnoItem item : listaAlumnos) {
                        String nombre = item.getNombre().toLowerCase().trim();
                        String matricula = item.getMatricula().toLowerCase().trim();
                        if (nombre.contains(busqueda) || matricula.contains(busqueda))
                            listaFiltrada.add(item);
                    }
                } else {
                    listaFiltrada.addAll(listaAlumnos);
                }

                resultado.values = listaFiltrada;
                resultado.count = listaFiltrada.size();

                return resultado;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults resultado) {
                listaFinal = (ArrayList<AlumnoItem>) resultado.values; // Actualizar la lista filtrada con los resultados
                notifyDataSetChanged();
            }
        };
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtNombre = (TextView) view.findViewById(R.id.txtAlumnoNombre);
            txtMatricula = (TextView) view.findViewById(R.id.txtMatricula);
            txtCarrera = (TextView) view.findViewById(R.id.txtCarrera);
            idImagen = (ImageView) view.findViewById(R.id.foto);

        }
    }
}