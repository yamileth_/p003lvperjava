package Modelo;

import com.example.p003lvperjava.AlumnoItem;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertAlumno(AlumnoItem alumno);
    public long updateAlumno(AlumnoItem alumno);
    public void deleteAlumno(int id);
}
