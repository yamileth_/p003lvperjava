package com.example.p003lvperjava;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.AlumnosDb;


public class Aplicacion extends Application {
    public static ArrayList<AlumnoItem> alumnos;
    public static AdapterAlumno adaptador;

    static AlumnosDb alumnosDb;

    public ArrayList<AlumnoItem> getAlumnos(){return alumnos;}
    public AdapterAlumno getAdaptador(){return adaptador;}

    @Override
    public void onCreate() {
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        alumnos = alumnosDb.allAlumnos();
        //alumnos = Alumno.llenarAlumnos();
        alumnosDb.openDataBase();
        adaptador = new AdapterAlumno(alumnos, this);
        Log.d("", "onCreate: tamaño array list " + alumnos.size());

    }
}