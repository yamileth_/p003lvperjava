package com.example.p003lvperjava;

import java.io.Serializable;

public class AlumnoItem implements Serializable {
    public String img;
    public String nombre;
    public String matricula;
    public String carrera;
    public int id;


    public AlumnoItem() {
        this.carrera = "";
        this.matricula = "";
        this.nombre = "";
        this.img = "";
    }

    public AlumnoItem(int id, String img, String nombre, String matricula, String carrera) {
        this.id = id;
        this.img = img;
        this.nombre = nombre;
        this.matricula = matricula;
        this.carrera = carrera;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   /* public static ArrayList<AlumnoItem> llenarAlumnos() {
        ArrayList<AlumnoItem> lst = new ArrayList<>();
        lst.add(new AlumnoItem(1, R.drawable.moruazamudio, "MORUA ZAMUDIO ESTEFANO", "2019030344", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(2, R.drawable.carranza, "CARRANZA JAUREGUI CARLOS ALBERTO", "2020030174", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(3, R.drawable.castrolopez2020030176, "CASTRO LOPEZ MARCO ANTONIO ALARID", "2020030176", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(4, R.drawable.duranvaldez2020030181, "DURAN VALDEZ JOSHUA DANIEL", "2020030181", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(5, R.drawable.galindo, "GALINDO HERNANDEZ ERNESTO DAVID", "2020030184", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(6, R.drawable.contreras2020030189, "CONTRERAS CEPEDA MAXIMILIANO", "2020030189", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(7, R.drawable.gomezruelas, "GOMEZ RUELAS IVÁN ENRIQUE", "2020030199", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(8, R.drawable.cruzquintero, "CRUZ QUINTERO JESUS EDUARDO", "2020030212", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(9, R.drawable.velardeovalle, "VELARDE OVALLE DAVID ANTONIO", "2020030241", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(10, R.drawable.lamasarmenta, "LAMAS ARMENTA GUSTAVO ADOLFO", "2020030243", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(11, R.drawable.rivaslugo, "RIVAS LUGO JUAN CARLOS", "2020030249", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(12, R.drawable.salasmendoza, "SALAS MENDOZA ALEJO", "2020030264", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(13, R.drawable.serrano, "SERRANO TORRES CARLOS JAIR", "2020030268", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(14, R.drawable.tirado, "TIRADO ROMERO JESUS TADEO", "2020030292", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(15, R.drawable.carillo, "CARRILLO GARCIA JAIR", "2020030304", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(16, R.drawable.arias, "ARIAS ZATARAIN DIEGO", "2020030306", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(17, R.drawable.valdezmartinez, "VALDEZ MARTINEZ PAOLA EMIRET", "2020030313", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(18, R.drawable.ibarraflores, "IBARRA FLORES SALMA YARETH", "2020030315", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(19, R.drawable.lizarragamaldonado, "LIZARRAGA MALDONADO JUAN ANTONIO", "2020030322", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(20, R.drawable.vieraromero, "VIERA ROMERO ANGEL ZINEDINE ANASTACIO", "2020030325", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(21, R.drawable.blas, "TEJEDA PEINADO BLAS ALBERTO", "2020030327", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(22, R.drawable.vieraromer2, "VIERA ROMERO ANGEL RONALDO ANASTACIO", "2020030329", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(23, R.drawable.elizalde, "ELIZALDE VARGAS XIOMARA YAMILETH", "2020030332", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(24, R.drawable.salcido, "SALCIDO SARABIA JESUS ANTONIO", "2020030333", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(25, R.drawable.sanchezyenifer, "RODRIGUEZ SANCHEZ YENNIFER CAROLINA", "2020030389", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(26, R.drawable.floresprado, "FLORES PRADO MANUEL ALEXIS", "2020030766", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(27, R.drawable.aguirretostado, "AGUIRRE TOSTADO VICTOR MOISES", "2020030771", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(28, R.drawable.dominguez, "DOMINGUEZ SARABIA HALACH UINIC", "2020030777", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(29, R.drawable.maciel, "MACIEL NUÑEZ ENZO ALEJANDRO", "2020030799", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(30, R.drawable.barron, "BARRON VARGAS JOSE ALBERTO", "2020030808", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(31, R.drawable.martinibarra, "MARTIN IBARRA GIANCARLO", "2020030819", "Ing. Tec. Informacion"));
        lst.add(new AlumnoItem(31, R.drawable.ocegueda, "SANCHEZ OCEGUEDA LUIS ANGEL", "2020030865", "Ing. Tec. Informacion"));
        return lst;
    }*/
}
