package com.example.p003lvperjava;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fbtnAgregar, fbtnSalir;
    private Aplicacion app;
    private AlumnoItem alumno;
    private int posicion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.alumno = new AlumnoItem();
        this.posicion = -1;
        this.app = (Aplicacion) getApplication();
        layoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.recId);
        recyclerView.setAdapter(app.getAdaptador());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        fbtnAgregar = findViewById(R.id.agregarAlumno);
        fbtnSalir = findViewById(R.id.btnSalir);

        fbtnSalir.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Alumnos");
            builder.setMessage("¿Seguro de que deseas salir?");
            builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.setNegativeButton("No", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        fbtnAgregar.setOnClickListener(v -> {
            alumno = null;
            Bundle bundle = new Bundle();
            bundle.putSerializable("alumno", alumno);
            bundle.putInt("posicion", posicion);
            Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
            intent.putExtras(bundle);

            startActivityForResult(intent, 0);
        });

        app.getAdaptador().setOnClickListener(v -> {
            posicion = recyclerView.getChildAdapterPosition(v);
            alumno = app.getAlumnos().get(posicion);
            Bundle bundle = new Bundle();
            bundle.putSerializable("alumno", alumno);
            bundle.putInt("posicion", posicion);

            Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
            intent.putExtras(bundle);

            startActivityForResult(intent, 0);

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        recyclerView.getAdapter().notifyDataSetChanged();
        this.posicion = -1;
        this.alumno = null;
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflador = getMenuInflater();
        inflador.inflate(R.menu.search_view, menu);
        MenuItem itemBuscar = menu.findItem(R.id.menu_search);
        SearchView buscador = (SearchView) itemBuscar.getActionView();
        buscador.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) { return false; }
            @Override
            public boolean onQueryTextChange(String s) {
                app.getAdaptador().getFilter().filter(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}